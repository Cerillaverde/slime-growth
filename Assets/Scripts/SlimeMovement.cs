using System;
using UnityEngine;
using UnityEngine.UI;

public class SlimeMovement : MonoBehaviour
{
    private Vector2 _startPosition;
    private Vector2 _endPosition;
    private bool _isDragging;
    private Rigidbody2D _rigidbody;
    private SpriteRenderer _spriteRenderer;
    
    [SerializeField] private float forceMultiplier;
    [SerializeField] private float forceMax;
    [SerializeField] private float maxDragDistance;
    [SerializeField] private float bounceForce;
    [SerializeField] private Image arrowIndicator;
    [SerializeField] private Gradient arrowColorGradient;
    [SerializeField] private WindControl windControl;

    public bool IsDragging { get => _isDragging; set => _isDragging = value; }
    public Rigidbody2D Rigidbody { get => _rigidbody; set => _rigidbody = value; }

    private Vector2 _cacheVelocity;
    private float _idleTimer;
    private bool _isCollision;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && _rigidbody.velocity.y == 0)
        {
            _startPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (Vector2.Distance(_startPosition, _rigidbody.position) < 1f)
                _isDragging = true;
        }
        
        if (Input.GetMouseButton(0) && _isDragging)
        {
            Vector2 currentMousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 direction = _startPosition - currentMousePosition;
            if (direction.x < 0)
                _spriteRenderer.flipX = true;
            else
                _spriteRenderer.flipX = false;
            
            UpdateArrow(direction);
        }
        
        if (Input.GetMouseButtonUp(0) && _isDragging)
        {
            _endPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 launchDirection = _startPosition - _endPosition;
            if (launchDirection.magnitude > maxDragDistance)
                launchDirection = launchDirection.normalized * maxDragDistance;

            float forceMagnitude = launchDirection.magnitude * forceMultiplier;
            if (forceMultiplier > forceMax)
                forceMagnitude = forceMax;
            
            _rigidbody.AddForce(launchDirection.normalized * forceMagnitude, ForceMode2D.Impulse);
            _isDragging = false;
            
            arrowIndicator.enabled = false;
        }

        if (Input.GetMouseButtonDown(1) && _isDragging)
            _isDragging = false;

        _cacheVelocity = _rigidbody.velocity;
        
        UpdateSpriteFlip();
    }

    private void FixedUpdate()
    {
        ApplyWindForce();
    }

    private void ApplyWindForce()
    {
        if (_rigidbody.velocity.y != 0 && !_isCollision)
            _rigidbody.AddForce(windControl.GetWindForce(), ForceMode2D.Force);
    }

    private void UpdateArrow(Vector2 direction)
    {
        arrowIndicator.enabled = true;

        Vector2 arrowPosition = _rigidbody.position + new Vector2(0, 0.1f);
        arrowIndicator.rectTransform.position = Camera.main.WorldToScreenPoint(arrowPosition);

        float angle = Mathf.Atan2(direction.y, -direction.x) * Mathf.Rad2Deg;
        arrowIndicator.rectTransform.rotation = Quaternion.Euler(new Vector3(0, 0, -angle));

        float scale = Mathf.Min(direction.magnitude, maxDragDistance);
        arrowIndicator.rectTransform.localScale = new Vector3(scale * 1.5f, arrowIndicator.rectTransform.localScale.y, arrowIndicator.rectTransform.localScale.z);
        
        float t = Mathf.Clamp01(direction.magnitude / maxDragDistance);
        arrowIndicator.color = arrowColorGradient.Evaluate(t);
    }

    private void UpdateSpriteFlip()
    {
        if (_cacheVelocity != Vector2.zero && _cacheVelocity.x < 0)
            _spriteRenderer.flipX = true;
        else if (_cacheVelocity != Vector2.zero && _cacheVelocity.x >= 0)
            _spriteRenderer.flipX = false;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        _isCollision = true;
        foreach (ContactPoint2D contact in other.contacts)
        {
            if (IsSideCollision(contact))
            {
                ApplyBounce(contact.normal);
                break;
            }
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        _isCollision = false;
    }

    private void ApplyBounce(Vector2 normal)
    {
        _rigidbody.AddForce(new Vector2(normal.x * bounceForce,  _cacheVelocity.y), ForceMode2D.Impulse);
    }

    private bool IsSideCollision(ContactPoint2D contact)
    {
        Vector2 contactPoint = contact.point - (Vector2)transform.position;

        float relativeDirection = Vector2.Dot(contactPoint.normalized, Vector2.right);
        float sideThreshold = 0.9f;

        return Mathf.Abs(relativeDirection) > sideThreshold;
    }
    
    public bool IsVelocityZeroForDuration(float duration)
    {
        if (_rigidbody.velocity.y == 0)
        {
            _idleTimer += Time.deltaTime;
            if (_idleTimer >= duration)
            {
                _idleTimer = 0f;
                return true;
            }
        }
        else
        {
            _idleTimer = 0f;
        }

        return false;
    }
    
}
