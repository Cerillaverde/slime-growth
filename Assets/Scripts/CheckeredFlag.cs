using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CheckeredFlag : MonoBehaviour
{
    [SerializeField] private GameObject text;
    private Vector3 _targetScale = new Vector3(1.7f, 0.7f);
    private float _duration = 1.2f; 

    private Vector3 _originalScale;

    private void Start()
    {
        _originalScale = text.transform.localScale;
        StartCoroutine(LoopScaleCoroutine());
    }

    private IEnumerator LoopScaleCoroutine()
    {
        while (true)
        {
            yield return StartCoroutine(ScaleCoroutine(_originalScale, _targetScale));
            yield return StartCoroutine(ScaleCoroutine(_targetScale, _originalScale));
        }
    }

    private IEnumerator ScaleCoroutine(Vector3 startScale, Vector3 endScale)
    {
        float time = 0;
        while (time < _duration)
        {
            text.transform.localScale = Vector3.Lerp(startScale, endScale, time / _duration);
            time += Time.deltaTime;
            yield return null;
        }
        text.transform.localScale = endScale;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Slime"))
            SceneManager.LoadScene(1);
    }

}
