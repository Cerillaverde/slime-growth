using System;
using UnityEngine;

public class PlatformDelete : MonoBehaviour
{
    private void Update()
    {
        if (transform.position.y < -10)
            Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        GetComponent<Rigidbody2D>().gravityScale = .2f;
    }
}
