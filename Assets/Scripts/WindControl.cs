using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class WindControl : MonoBehaviour
{

    [SerializeField] private Image flag;
    [SerializeField] private Sprite flag1;
    [SerializeField] private Sprite flag2;
    [SerializeField] private Sprite flag3;
    private Vector2 _windDirection;
    private float _windStrength;

    private void Start()
    {
        _windDirection = Vector2.zero;
        _windStrength = 0;
        StartCoroutine(WindChanged());
    }

    public Vector2 GetWindForce()
    {
        return _windDirection.normalized * Mathf.Abs(_windStrength);
    }

    private IEnumerator WindChanged()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(3, 11));

            _windStrength += Random.Range(-2f, 2f);
            _windStrength = Mathf.Clamp(_windStrength, -4.5f, 4.5f);

            _windDirection = _windStrength > 0 ? Vector2.right : Vector2.left;
            Debug.Log(_windStrength);
            switch (Mathf.Abs(_windStrength))
            {
                case < 1f:
                    flag.sprite = flag1;
                    break;
                case <= 3f:
                    flag.sprite = flag2;
                    break;
                case > 3f:
                    flag.sprite = flag3;
                    break;
            }
            flag.transform.rotation = _windStrength < 0 ? Quaternion.Euler(0, 180, 0) : Quaternion.Euler(0,0,0);
        }
    }
}
