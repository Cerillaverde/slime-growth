using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlatformControl : MonoBehaviour
{
    [SerializeField] private float speed;
    private Rigidbody2D _rigidbody;
    
    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _rigidbody.isKinematic = true;
        _rigidbody.constraints = RigidbodyConstraints2D.FreezePositionY | RigidbodyConstraints2D.FreezeRotation;
        _rigidbody.gravityScale = 0;
        _rigidbody.velocity = Vector2.right * speed;
    }

    private void FixedUpdate()
    {
        if (transform.position.x > 7f)
            _rigidbody.velocity = Vector2.left * speed;
        else if (transform.position.x < -7f)
            _rigidbody.velocity = Vector2.right * speed;
        
    }
}
