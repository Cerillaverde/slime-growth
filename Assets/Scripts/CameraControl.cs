using System;
using UnityEngine;

public class CameraControl : MonoBehaviour
{

    private Camera _mainCamera;
    private float _lastPositionY;
    private Transform _slimeTransform;
    private Vector3 _targetPosition;
    private bool _isMovingToTarget;

    private void Awake()
    {
        _mainCamera = Camera.main;
        _slimeTransform = GetComponent<Transform>();
    }

    private void Start()
    {
        _lastPositionY = _slimeTransform.position.y;
    }

    private void FixedUpdate()
    {
        if (_slimeTransform.position.y < _lastPositionY - 2.2f)
        {
            Vector3 newCameraPosition = _mainCamera.transform.position;
            newCameraPosition.y = _slimeTransform.position.y + 2f;
            _mainCamera.transform.position = newCameraPosition;
        }
        
        if (_isMovingToTarget)
        {
            _mainCamera.transform.position = Vector3.Lerp(_mainCamera.transform.position, _targetPosition, 5f * Time.deltaTime);

            if (Vector3.Distance(_mainCamera.transform.position, _targetPosition) < 0.01f)
            {
                _mainCamera.transform.position = _targetPosition;
                _isMovingToTarget = false;
                _lastPositionY = _slimeTransform.position.y;
            }
        }
    }

    public void OnFloor()
    {
        _targetPosition = _mainCamera.transform.position;
        _targetPosition.y = _slimeTransform.position.y + 2f;
        _isMovingToTarget = true;
        
    }
    
}
