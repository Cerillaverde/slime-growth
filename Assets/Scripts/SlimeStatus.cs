using System;
using System.Collections;
using Enum;
using UnityEngine;

public class SlimeStatus : MonoBehaviour
{
    private SlimeMovement _slimeMovement;
    private EMachineState _currentState;
    private SpriteRenderer _spriteRenderer;
    private CameraControl _cameraControl;
    [SerializeField] private Sprite idleSprite1;
    [SerializeField] private Sprite idleSprite2;
    [SerializeField] private Sprite draggingSprite;
    [SerializeField] private Sprite jumpingSprite;

    private void Awake()
    {
        _slimeMovement = GetComponent<SlimeMovement>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _cameraControl = GetComponent<CameraControl>();
    }

    private void Start()
    {
        _currentState = EMachineState.Idle;
        InitState(_currentState);
    }

    private void Update()
    {
        UpdateState();
    }

    private void ChangeState(EMachineState newState)
    {
        if (newState == _currentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(EMachineState currentState)
    {
        _currentState = currentState;
        switch (_currentState)
        {
            case EMachineState.Idle:
                _cameraControl.OnFloor();
                StartCoroutine(SwapSpriteIdle());
                break;
            
            case EMachineState.Dragging:
                _spriteRenderer.sprite = draggingSprite;
                break;
            
            case EMachineState.Jumping:
                _spriteRenderer.sprite = jumpingSprite;
                break;
        }
    }

    private void ExitState()
    {
        switch (_currentState)
        {
            case EMachineState.Idle:
                StopAllCoroutines();
                break;
        }
    }

    private void UpdateState()
    {
        switch (_currentState)
        {
            case EMachineState.Idle:
                if (_slimeMovement.Rigidbody.velocity.y != 0)
                    ChangeState(EMachineState.Jumping);
                
                if (_slimeMovement.IsDragging)
                    ChangeState(EMachineState.Dragging);
                
                break;
            
            case EMachineState.Jumping:
                if (_slimeMovement.IsVelocityZeroForDuration(0.05f))
                    ChangeState(EMachineState.Idle);
                
                break;
            
            case EMachineState.Dragging:
                if (!_slimeMovement.IsDragging)
                    ChangeState(EMachineState.Idle);

                break;
        }
    }

    private IEnumerator SwapSpriteIdle()
    {
        while (true)
        {
            _spriteRenderer.sprite = idleSprite1;
            yield return new WaitForSeconds(0.7f);
            _spriteRenderer.sprite = idleSprite2;
            yield return new WaitForSeconds(0.3f);
        }
    }
}
